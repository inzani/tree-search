import java.util.ArrayList;

/**
 * @author Alexey
 * @version 1.0
 */
public class PhraseOMatch {
    public static void main(String[] args){
        Node<String> nodeOne = new Node<String>("One");
        Node<String> nodeTwo = new Node<String>("Two");
        Node<String> nodeThree = new Node<String>("Three", nodeTwo);
        Node<String> nodeFour = new Node<String>("Four",nodeThree);
        Node<String> nodeFinnaly = new Node<String>("Final",nodeOne, nodeFour);

        ArrayList<String> Nodes = nodeFinnaly.GetValue();

        for(var node:Nodes){
            System.out.println(node);
        }

        Node<String> master = nodeFinnaly.GetMasterNodes(nodeOne);
        System.out.println(master.Value);

    }
}