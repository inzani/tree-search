import java.util.ArrayList;
public class Node<T>{
    public Node<T>[] Nodes;
    public T Value;

    private ArrayList<T> listValue = new ArrayList<T>();

    public Node(T value, Node<T>... nodes){
        this.Nodes = nodes;
        this.Value = value;
    }

    public Node<T>[] getNodes() {
        return Nodes;
    }

    private Node<T>[] setNodes(Node<T>[] node){
        this.Nodes = node;
        return node;
    }

    public T setValue(){
        return Value;
    }

    public T getValue(T Value){
        this.Value = Value;
        return Value;
    }

    public ArrayList<T> GetValue(){
        return GetValues(this);

    }

    private ArrayList<T> GetValues(Node<T> nodes){
        if(nodes == this) listValue.add(nodes.Value);
        for (var node:nodes.Nodes) {
            listValue.add(node.Value);
            GetValues(node);
        }
        return listValue;
    }

    public Node<T> GetMasterNodes(Node<T> findNode){
        return GetMasterNode(findNode,null);
    }

    private Node<T> GetMasterNode(Node<T> findNode,Node<T> masterNode){
        if(this == findNode) return masterNode;
        for(var node : Nodes){
            var result = node.GetMasterNode(findNode,this);
            if (result!=null) return result;
        }
        return null;
    }
}